package com.practicas.miguel.reservasaulas.libreria;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

/**
 * Created by Miguel on 29/10/2015.
 */
public class FicherosRed {


   public static String ETIQUETA="Ficheros en red";

    /***
     *
     * @param texto
     * @return
     */
    public static Resultado conectarJava(String texto) {
        URL url;
        HttpURLConnection urlConnection = null;
        int respuesta=0;
        Resultado resultado = new Resultado();
        try {
            url = new URL(texto);
            urlConnection = (HttpURLConnection) url.openConnection();
            try {
                respuesta = urlConnection.getResponseCode();
            } catch (Exception e){
                e.getMessage();
            }
            if (respuesta == HttpURLConnection.HTTP_OK) {
                resultado.setCodigo(true);
                resultado.setContenido(leer(urlConnection.getInputStream()));
            } else {
                resultado.setCodigo(false);
                resultado.setMensaje("Error en el acceso a la web: " + String.valueOf(respuesta));
            }
        } catch (IOException e) {
            Log.e(ETIQUETA, "Error leyendo HTTP");
            resultado.setCodigo(false);
            resultado.setMensaje("Excepción: " + e.getMessage());
        } finally {
            try {
                if (urlConnection != null)
                    urlConnection.disconnect();
            } catch (Exception e) {
                resultado.setCodigo(false);
                resultado.setMensaje("Excepción: " + e.getMessage());
            }
            return resultado;
        }
    }


    public static Resultado conectarApache(String texto){

        CloseableHttpClient cliente = null;
        HttpPost peticion;

        HttpResponse respuesta;
        int valor;
        Resultado resultado = new Resultado();
        try {
//cliente = new DefaultHttpClient();
            cliente = HttpClientBuilder.create().build();
            peticion = new HttpPost(texto);
            respuesta = cliente.execute(peticion);
            valor = respuesta.getStatusLine().getStatusCode();
            if (valor == HttpURLConnection.HTTP_OK) {
                resultado.setCodigo(true);
                resultado.setContenido(leer(respuesta.getEntity().getContent()));
            }
            else {
                resultado.setCodigo(false);
                resultado.setMensaje("Error en el acceso a la web: " + String.valueOf(valor));
            }
            cliente.close();
        } catch (IOException e) {
            resultado.setCodigo(false);
            resultado.setMensaje("Excepción: " + e.getMessage());
            if (cliente != null)
                try {
                    cliente.close();
                } catch (IOException excep) {
                    resultado.setCodigo(false);
                    resultado.setMensaje("Excepción: " + excep.getMessage());
                }
        }
        return resultado;
    }


    public static void conectarAsyncHttp(String url, final ConnectionListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new TextHttpResponseHandler() {
                    @Override
                    public void onStart() {


                    }
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {

                        Resultado res = new Resultado();
                        res.setCodigo(true);
                        res.setContenido(response);
                        res.setMensaje(statusCode + "");
                        listener.onConnectionEnd(res);

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                        Resultado res = new Resultado();
                        res.setCodigo(false);
                        res.setContenido(response);
                        res.setMensaje(statusCode + "");
                        listener.onConnectionEnd(res);

                    }
                }
        );


    }

    public static void postAsyncHttp(String url, RequestParams paramsPost, final ConnectionListener connectionListener) {
        //final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        //client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.post(url, paramsPost, new TextHttpResponseHandler() {

                    @Override
                    public void onStart() {


                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                        Resultado res = new Resultado();
                        res.setCodigo(true);
                        res.setMensaje(statusCode + "");
                        res.setContenido(responseBody);
                        connectionListener.onConnectionEnd(res);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                        Resultado res = new Resultado();
                        res.setCodigo(false);
                        res.setMensaje(statusCode + "");
                        res.setContenido(responseBody.toString());
                        connectionListener.onConnectionEnd(res);
                    }
                }

        );
    }


    public static void putAsyncHttp(String url, RequestParams paramsPost, final ConnectionListener connectionListener) {
        //final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        //client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.put(url, paramsPost, new TextHttpResponseHandler() {

            @Override
            public void onStart() {


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                Resultado res = new Resultado();
                res.setCodigo(true);
                res.setMensaje(statusCode + "");
                res.setContenido(responseBody);
                connectionListener.onConnectionEnd(res);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                Resultado res = new Resultado();
                res.setCodigo(false);
                res.setMensaje(statusCode + "");
                res.setContenido(responseBody.toString());
                connectionListener.onConnectionEnd(res);
            }
        });


    }

    public static void delAsyncHttp(String url, RequestParams paramsPost, final ConnectionListener connectionListener) {
        //final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        //client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

        client.delete(url, paramsPost, new TextHttpResponseHandler() {

                    @Override
                    public void onStart() {


                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                        Resultado res = new Resultado();
                        res.setCodigo(true);
                        res.setMensaje(statusCode + "");
                        res.setContenido(responseBody);
                        connectionListener.onConnectionEnd(res);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                        Resultado res = new Resultado();
                        res.setCodigo(true);
                        res.setMensaje(statusCode + "");
                        res.setContenido(responseBody.toString());
                        connectionListener.onConnectionEnd(res);
                    }
                }
        );


    }


    private static String leer(InputStream inputStream) throws IOException  {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String linea= r.readLine();

        while ((linea != null)) {
            total.append(linea);
            linea= r.readLine();
        }

        return total.toString();
    }


    public static boolean conexion(Context c){

        ConnectivityManager connectivityManager = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo i = connectivityManager.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;

    }




}
