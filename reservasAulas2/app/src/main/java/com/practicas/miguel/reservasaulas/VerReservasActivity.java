package com.practicas.miguel.reservasaulas;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.practicas.miguel.reservasaulas.libreria.ConnectionListener;
import com.practicas.miguel.reservasaulas.libreria.FicherosRed;
import com.practicas.miguel.reservasaulas.libreria.Resultado;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class VerReservasActivity extends Activity implements ConnectionListener {

    public static String URLRESERVAS = "http://192.168.1.134/html/slimrest/api/v1/reservas";
    String FILE_RESERVAS = "reservas";
    ListView l;
    ArrayList<Reserva> listaReservas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_reservas);
        l = (ListView) findViewById(R.id.MRLverReservas_lv);
        listaReservas = new ArrayList<Reserva>();
        obtenerDatos(URLRESERVAS);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ver_reservas, menu);
        return true;
    }

    public void obtenerDatos(String url){

         FicherosRed.conectarAsyncHttp(url,this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void analizarResultado(Resultado resultado) throws JSONException {


        JSONObject jsonObjeto = new JSONObject(resultado.getContenido());


        JSONArray reservas = jsonObjeto.getJSONArray("reservas");

        for (int i = 0;i<reservas.length();++i){
            Reserva r = new Reserva();
            JSONObject json = reservas.getJSONObject(i);
            r.setId(json.getInt("ID"));
            r.setCodAula(json.getInt("AULA"));
            r.setCodProfesor(json.getInt("PROFESOR"));
            r.setFecha(CrearReservaActivity.traducirFechaEspañol(json.getString("FECHA")));
            r.setHoraInicio(json.getString("HORA_INICIO"));
            r.setHoraFinal(json.getString("HORA_FIN"));
            listaReservas.add(r);
        }

    }


    public void mostrarResultado(){

        String [] reservas = new String [listaReservas.size()];

        //Basura de método
       /* int i =0;
        for (Reserva r: listaReservas){
            reservas[i] = r.toString();
            ++i;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, reservas);
        l.setAdapter(adapter);
*/
        AdaptadorFila adaptador = new AdaptadorFila(this,listaReservas);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, partidos);

        l.setAdapter(adaptador);

    }

    @Override
    public void onConnectionEnd(Resultado resultado) {

        if (resultado.getCodigo()){
            try {
                analizarResultado(resultado);
                mostrarResultado();
            } catch (JSONException e) {
                e.printStackTrace();
                listaReservas = MainActivity.getInstanceReservas();
                mostrarResultado();
            }

        } else {
            listaReservas = MainActivity.getInstanceReservas();
            mostrarResultado();
        }


    }
}
