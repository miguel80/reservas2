package com.practicas.miguel.reservasaulas;

import java.io.Serializable;

/**
 * Created by gutiEuler on 09/06/2016.
 */
public class Reserva implements Serializable{

    private int id;
    private int codProfesor;
    private int codAula;
    private String fecha;
    private String horaInicio;
    private String horaFinal;

    public Reserva() {

    }

    public Reserva(int id, int codProfesor, int codAula, String fecha, String horaInicio, String horaFinal) {
        this.id = id;
        this.codProfesor = codProfesor;
        this.codAula = codAula;
        this.fecha = fecha;
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    public int getCodProfesor() {
        return codProfesor;
    }

    public void setCodProfesor(int codProfesor) {
        this.codProfesor = codProfesor;
    }

    public int getCodAula() {
        return codAula;
    }

    public void setCodAula(int codAula) {
        this.codAula = codAula;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    @Override
    public String toString() {
        return "Reserva{" +
                "id=" + id +
                ", codProfesor=" + codProfesor + '\n' +
                ", codAula=" + codAula + '\n' +
                ", fecha='" + fecha + '\'' + '\n' +
                ", horaInicio='" + horaInicio + '\'' +
                ", horaFinal='" + horaFinal + '\'' +
                '}';
    }
}
