package com.practicas.miguel.reservasaulas.libreria;


/**
 * Created by miguel
 */
public interface ConnectionListener {

    public void onConnectionEnd(Resultado resultado);

}
