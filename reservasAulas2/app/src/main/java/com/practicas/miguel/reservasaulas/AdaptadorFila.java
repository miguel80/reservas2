package com.practicas.miguel.reservasaulas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorFila extends BaseAdapter {
    private ArrayList<Reserva> reservas;
    private Context context;

    public AdaptadorFila(Context context, ArrayList<Reserva> values) {
        this.context = context;
        this.reservas = values;
    }
    @Override
    public int getCount() {
        return reservas.size();
    }
    @Override
    public String getItem(int position) {
        return reservas.get(position).toString();
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View adapterView = convertView;
        if (adapterView == null) {
            adapterView = inflater.inflate(R.layout.fila_reserva, null);
        }
        TextView text1 = (TextView) adapterView.findViewById(R.id.FRreservaId_tv);
        text1.setText("Id: " + reservas.get(position).getId() + "");
        TextView text2 = (TextView) adapterView.findViewById(R.id.FRcodigoProfesor_tv);
        text2.setText("Profesor: " + reservas.get(position).getCodProfesor() + "");
        TextView text3 = (TextView) adapterView.findViewById(R.id.FRcodigoaula_tv);
        text3.setText("Aula: " + reservas.get(position).getCodAula()+"") ;
        TextView text4 = (TextView) adapterView.findViewById(R.id.FRfecha_tv);
        text4.setText("Fecha: " + reservas.get(position).getFecha());
        TextView text5 = (TextView) adapterView.findViewById(R.id.FRhorainicio_tv);
        text5.setText("Hora Inicio: " + reservas.get(position).getHoraInicio());



        return adapterView;
    }

    public void limpiarDatos(){
        reservas.clear();
    }

    public void setReservas(ArrayList<Reserva> reservas) {
        this.reservas = (ArrayList<Reserva>) reservas.clone();
    }
}
