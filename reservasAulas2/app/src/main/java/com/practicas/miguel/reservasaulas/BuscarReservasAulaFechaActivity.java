package com.practicas.miguel.reservasaulas;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.practicas.miguel.reservasaulas.libreria.ConnectionListener;
import com.practicas.miguel.reservasaulas.libreria.FicherosRed;
import com.practicas.miguel.reservasaulas.libreria.Resultado;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

public class BuscarReservasAulaFechaActivity extends Activity implements ConnectionListener, View.OnClickListener {

    public static String  URL_RESERVAS = "http://192.168.1.134/html/slimrest/api/v1/reservas";
    EditText aula, fecha;
    TextView res;
    Button buscar;
    ArrayList<Reserva> listaReservas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_reservas_aula_fecha);

        aula = (EditText) findViewById(R.id.RAFaula_et);
        fecha = (EditText) findViewById(R.id.RAFfecha_et);
        buscar = (Button) findViewById(R.id.RAFbuscar_btn);
        res = (TextView) findViewById(R.id.RAFres_et);

        buscar.setOnClickListener(this);

        listaReservas = new ArrayList<Reserva>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ver_reservas, menu);
        return true;
    }

    public void obtenerDatos(String url){

        String urlDatos = url + "/" + CrearReservaActivity.traducirFechaIngles(fecha.getText()+"") + "/" + aula.getText();
        FicherosRed.conectarAsyncHttp(urlDatos, this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void analizarResultado(Resultado resultado) throws JSONException {


        JSONObject jsonObjeto = new JSONObject(resultado.getContenido());


        JSONArray reservas = jsonObjeto.getJSONArray("reservas");

        for (int i = 0;i<reservas.length();++i){
            Reserva r = new Reserva();
            JSONObject json = reservas.getJSONObject(i);
            r.setId(json.getInt("ID"));
            r.setCodAula(json.getInt("AULA"));
            r.setCodProfesor(json.getInt("PROFESOR"));
            r.setFecha(json.getString("FECHA"));
            r.setHoraInicio(json.getString("HORA_INICIO"));
            r.setHoraFinal(json.getString("HORA_FIN"));
            listaReservas.add(r);
        }

    }


    public void mostrarResultado(){

        String resFinal = "Horas reservadas para el aula " + aula.getText() + ": \n";

        //Basura de método
        for (Reserva r: listaReservas){
            resFinal = resFinal + r.getHoraInicio() + " - " + r.getHoraFinal() + "\n";
        }

        res.setText(resFinal);



    }

    @Override
    public void onConnectionEnd(Resultado resultado) {

        if (resultado.getCodigo()){
            try {
                analizarResultado(resultado);
                mostrarResultado();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

        }


    }


    @Override
    public void onClick(View v) {

        if (v == buscar){
            obtenerDatos(URL_RESERVAS);
        }

    }
}
