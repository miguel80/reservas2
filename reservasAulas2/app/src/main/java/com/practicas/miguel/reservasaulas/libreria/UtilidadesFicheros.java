package com.practicas.miguel.reservasaulas.libreria;

/**
 * Created by Miguel on 31/03/2016.
 */
//http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UtilidadesFicheros {

    public static String ETIQUETA = "UtilidadesFicheros";


    public static void leerRawFile(){

    }

    public static void escribirFicheroMemoriaInternaFOS(Context contexto, String nombreFichero, String texto){

        FileOutputStream fos = null;
        try {


            //fos = getApplicationContext().openFileOutput(nombreFichero,Context.MODE_PRIVATE);
            fos = contexto.openFileOutput(nombreFichero, Context.MODE_PRIVATE);
            fos.write(texto.getBytes());

        } catch (FileNotFoundException e) {
            Log.e(ETIQUETA,e.getMessage());
        } catch (IOException e) {
            Log.e(ETIQUETA, e.getMessage());
        } finally {
            try {
                if (fos != null)
                    fos.close();
            } catch (IOException e) {
                Log.e(ETIQUETA, e.getMessage());
            }
        }
    }




    public static void escribirFicheroMemoriaInternaBW(Context contexto, String cadena, String nombreFichero, String formato){


        File miFichero;
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter out = null;

        try {
            //mifichero = new File(getApplicationContext().getFilesDir(), nombreFichero);
            miFichero = new File(contexto.getFilesDir(), nombreFichero);
            fos = new FileOutputStream(miFichero,true);
            osw = new OutputStreamWriter(fos, formato);
            out = new BufferedWriter(osw, 32768); //tamaÃƒÂ±o del buffer de 32 kbytes
            out.write(cadena);
        } catch (IOException excep){
            Log.e(ETIQUETA, excep.getMessage());
        } finally {
            try {
                if (out!= null)
                    out.close();
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }


    }

    public String leerInternaFIS(Context contexto, String fichero){

        FileInputStream fis = null;
        StringBuilder miCadena = new StringBuilder();
        int n;
        boolean correcto = false;
        try {
            fis = contexto.getApplicationContext().openFileInput(fichero);
            //fichero = new File(getFilesDir(), fichero);
            //fis = new FileInputStream (miFichero) ;
            while ((n = fis.read()) != -1)
                miCadena.append((char) n);
        } catch (IOException e) {
            Log.e(ETIQUETA, e.getMessage());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    correcto = true;
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }

        if (correcto)
            return miCadena.toString();
        else
            return "0";
    }


    public static File abrirFicheroMemoriaInterna(Context contexto, String _fichero){
        File fichero;
        fichero = new File(contexto.getFilesDir(), _fichero);

        return fichero;
    }


    public static String mostrarPropiedadesInterna (Context contexto, String fichero) {

        File miFichero = abrirFicheroMemoriaInterna(contexto, fichero);
        return mostrarPropiedades(miFichero);
    }

    public static String mostrarPropiedadesExterna (String fichero) {
        File miFichero = abrirFicheroMemoriaExterna(fichero);

        return mostrarPropiedades(miFichero);
    }

    public static String mostrarPropiedades (File fichero) {
        SimpleDateFormat formato = null;
        StringBuffer txt = new StringBuffer();
        try {
            if (fichero.exists()) {
                txt.append("Nombre: " + fichero.getName() + '\n');
                txt.append("Ruta: " + fichero.getAbsolutePath() + '\n');
                txt.append("TamaÃƒÂ±o (bytes): " + Long.toString(fichero.length()) + '\n');
                formato = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
                txt.append("Fecha: " + formato.format(new Date(fichero.lastModified())) + '\n');
            } else
                txt.append("No existe el fichero " + fichero.getName() + '\n');
        } catch (Exception e) {
            Log.e(ETIQUETA, e.getMessage());
            txt.append(e.getMessage());
        }
        return txt.toString();
    }



    public static boolean disponibleEscrituraExterna(){
        boolean escritura = false;
        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String estado = Environment.getExternalStorageState();
        if (estado.equals(Environment.MEDIA_MOUNTED))
            escritura = true;
        return escritura;
    }

    public static boolean disponibleLecturaExterna(){
        boolean lectura = false;
        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String estado = Environment.getExternalStorageState();
        if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY)
                || estado.equals(Environment.MEDIA_MOUNTED))
            lectura = true;
        return lectura;
    }



    public static File abrirFicheroMemoriaExterna(String _fichero){

        File fichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        fichero = new File(tarjeta.getAbsolutePath(), _fichero);
        return fichero;
    }


    public static void escribirFicheroMemoriaExterna(String _fichero, String cadena, boolean anadir, String formato){


        File fichero = abrirFicheroMemoriaExterna(_fichero);

        FileOutputStream fos=null;
        PrintWriter pw=null;

        try {

            fos = new FileOutputStream(fichero,anadir);
            pw = new PrintWriter(fos);
            pw.print(cadena);
            pw.flush();
            pw.close();
            fos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e(ETIQUETA, "Fichero no encontrado");
        } catch (IOException e) {
            Log.e(ETIQUETA, "Error de entrada salida");
        }


    }



    public static Resultado leerFichero(File file, String codigo){

        Resultado resultado = new Resultado();
        StringBuilder texto = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String linea;

            while ((linea = br.readLine()) != null) {
                texto.append(linea);
                texto.append('\n');
            }
            br.close();

            resultado.setCodigo(true);
            resultado.setContenido(texto.toString());
        }
        catch (IOException e) {

            Log.e(ETIQUETA, "Error leyendo fichero: " + codigo);
            resultado.setCodigo(false);
            resultado.setMensaje(e.getMessage());
        }

        return resultado;

    }


    public static Resultado leerFicheroExterna(String fichero, String codigo){

        File miFichero = abrirFicheroMemoriaExterna(fichero);
        return leerFichero(miFichero, codigo);
    }

    public static Resultado leerFicheroInterna(Context contexto, String fichero, String codigo){

        File miFichero = abrirFicheroMemoriaInterna(contexto, fichero);
        return leerFichero(miFichero, codigo);

    }


    public static void escribirObjetoFichero(File file, Serializable object, Context context){
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(object);
            os.close();
            fos.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static <T> T cargarObjetoFichero(File file,  Context context) {



        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);
            T objeto = (T) is.readObject();
            is.close();
            fis.close();
            return objeto;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}

