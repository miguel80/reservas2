package com.practicas.miguel.reservasaulas;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.practicas.miguel.reservasaulas.libreria.ConnectionListener;
import com.practicas.miguel.reservasaulas.libreria.FicherosRed;
import com.practicas.miguel.reservasaulas.libreria.Resultado;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

public class CrearReservaActivity extends Activity implements View.OnClickListener, ConnectionListener {

    public static String URL_RESERVAS = "http://192.168.1.134/html/slimrest/api/v1/reservas";

    public static final String FORMATO_FECHA_ESPANOL = "dd-MM-yyyy";
    public static final String FORMATO_FECHA_INGLES = "yyyy-MM-dd";
    public static final String FECHA_INICIO = "01-01-2016";
    public static final String FECHA_FINAL = "24-06-2016";
    public static final String FECHA_INICIO_SBLANCA= "22-02-2016";
    public static final String FECHA_FINAL_SBLANCA = "29-02-2016";
    public static final String FECHA_INICIO_SSANTA= "21-03-2016";
    public static final String FECHA_FINAL_SSANTA = "28-03-2016";
    public static final int MAX_POSTERIOR = 30;
    public static final String [] horasValidas = {"8:15","9:15","10:15","11:45","12:45","13:45"};
    public static final int MAX_VECES_SEMANA = 2;
    public static final String RESERVA = "Reserva";

    EditText profesor, aula, fecha, hora, contrasena;
    Button crear;
    Reserva reserva;
    private ProgressDialog progreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_reserva);

        profesor = (EditText) findViewById(R.id.CRcodigoprofesor_et);
        aula  = (EditText) findViewById(R.id.CRcodigoaula_et);
        fecha = (EditText) findViewById(R.id.CRfecha_et);
        hora = (EditText) findViewById(R.id.CRhora_et);
        crear = (Button) findViewById(R.id.CRcrear_btn);
        contrasena = (EditText) findViewById(R.id.CRcontrasena_et);

        crear.setOnClickListener(this);

        Reserva res = (Reserva) getIntent().getSerializableExtra(RESERVA);

        if (res != null) {
            profesor.setText(res.getCodProfesor()+"");
            aula.setText(res.getCodAula()+"");
            fecha.setText(res.getFecha()+"");
            hora.setText(res.getHoraInicio()+"");

        }


    }

    public void mostrarBarraProgreso(){
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(false);
        progreso.show();
    }


    public static String sumaHora(String hora){
        try {
            String[] horaSplit = hora.split(":");
            String horaNueva = (Integer.parseInt(horaSplit[0]) + 1) % 24 + "";
            return horaNueva + ":" + horaSplit[1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crear_reserva, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static ArrayList<Pair<String, String>> generarHorariosEntreDosFechas (Date f1, Date f2){

        ArrayList<Pair<String, String>> horarios = new ArrayList<Pair<String,String>>();
        SimpleDateFormat dmy = new SimpleDateFormat("dd-MM-yyyy");
        int days = 0;
        while (!f1.after(f2)){
            if (fechaValida(dmy.format(f1))) {
                for (String h : horasValidas) {
                    horarios.add(new Pair(dmy.format(f1), h));
                }
            }
            f1 = fechaPosterior(f1,1);
        }


        return horarios;
    }

    public static Date parseDateEspanol(String d){

        DateFormat format = new SimpleDateFormat(FORMATO_FECHA_ESPANOL);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date parseDateIngles(String d){

        DateFormat format = new SimpleDateFormat(FORMATO_FECHA_INGLES);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static boolean fechaEntre(Date min, Date max, Date f){

        return f.after(min) && f.before(max);

    }


    public static Date fechaPosterior (Date date,int days){
        // Start date

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);  // number of days to add
        return c.getTime();
    }

    public static Date fechaPosterior (int days){
          // Start date

        DateFormat dateFormat = new SimpleDateFormat(FORMATO_FECHA_ESPANOL);
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);  // number of days to add
        return c.getTime();
    }

    public static boolean finDeSemana(Date d) {

        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        return dayOfWeek == 1 || dayOfWeek == 7;

    }

    public static  boolean posterior(String fecha, int maxPosterior) {

        Date fechaPosterior = fechaPosterior(maxPosterior);
        Date hoy = new Date();

        return fechaEntre(hoy, fechaPosterior, parseDateEspanol(fecha));

    }

    private void mostrarDialogo(String titulo, String mensaje){


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(titulo);
        alert.setMessage(mensaje);

        // Set an EditText view to get user input
        /*final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        alert.setView(input);*/

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //password = input.getText().toString();
                //Log.d( TAG, "Pin Value : " + value);

                return;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

               // password = null;
                return;
            }
        });
        alert.show();
    }



    public static boolean fechaValida (String fecha){
        String msgError = "";

        //Compruebo que la fecha está entre el 01/01/2016 - 24/06/2016
        if (! fechaEntre(parseDateEspanol(FECHA_INICIO), parseDateEspanol(FECHA_FINAL), parseDateEspanol(fecha))){
            msgError = msgError + "La fecha debe estar entre: " + FECHA_INICIO + " y el " + FECHA_FINAL + "\n";
        }

        //Compruebo que la fecha no sea ni semana blanca, ni semana santa
        if (fechaEntre(parseDateEspanol(FECHA_INICIO_SBLANCA), parseDateEspanol(FECHA_FINAL_SBLANCA), parseDateEspanol(fecha))||
                fechaEntre(parseDateEspanol(FECHA_INICIO_SSANTA), parseDateEspanol(FECHA_FINAL_SSANTA), parseDateEspanol(fecha))    ){

            msgError = msgError + "Durante la semana blanca o la semana santa no se pueden hacer reservas"+ "\n";
        }

        //Compruebo esté entre los 30 días naturales posteriores al día actual.
        if (!posterior(fecha, MAX_POSTERIOR)) {
            msgError = msgError + "La fecha de reserva debe estar a lo sumo " + MAX_POSTERIOR +" después de la fecha actual"+ "\n";
        }

        if (finDeSemana(parseDateEspanol(fecha))){
            msgError = msgError + "La fecha debe ser un día entre lunes y viernes"+ "\n";
        }

        return msgError=="";
    }

    public static String restriccionesLocales(Reserva res){

        String msgError = "";

        //Compruebo que la fecha está entre el 01/01/2016 - 24/06/2016
        if (! fechaEntre(parseDateEspanol(FECHA_INICIO), parseDateEspanol(FECHA_FINAL), parseDateEspanol(res.getFecha()))){
            msgError = msgError + "La fecha debe estar entre: " + FECHA_INICIO + " y el " + FECHA_FINAL + "\n";
        }

        //Compruebo que la fecha no sea ni semana blanca, ni semana santa
        if (fechaEntre(parseDateEspanol(FECHA_INICIO_SBLANCA), parseDateEspanol(FECHA_FINAL_SBLANCA), parseDateEspanol(res.getFecha()))||
                fechaEntre(parseDateEspanol(FECHA_INICIO_SSANTA), parseDateEspanol(FECHA_FINAL_SSANTA), parseDateEspanol(res.getFecha()))    ){

            msgError = msgError + "Durante la semana blanca o la semana santa no se pueden hacer reservas"+ "\n";
        }

        //Compruebo esté entre los 30 días naturales posteriores al día actual.
        if (!posterior(res.getFecha(), MAX_POSTERIOR)) {
            msgError = msgError + "La fecha de reserva debe estar a lo sumo " + MAX_POSTERIOR +" después de la fecha actual"+ "\n";
        }

        if (finDeSemana(parseDateEspanol(res.getFecha()))){
            msgError = msgError + "La fecha debe ser un día entre lunes y viernes"+ "\n";
        }

        //Hora entre 8:15 - 14:45 salvo el recreo (11:15-11:45)
        boolean valida = false;
        for (String s : horasValidas){
            if (s.equals(res.getHoraInicio())){
                valida = true;

            }
        }

        if (!valida){
            msgError = msgError + "La hora debe ser una hora de inicio de clase: "+ Arrays.asList(horasValidas)+ "\n";
        }
/* comprobado por trigger
        if (numeroReservasAulaSemana(res.getCodAula(), res.getFecha(),res.getCodProfesor())>MAX_VECES_SEMANA){
            msgError = "Tiene reservada el aula dos veces en la misma semana"+ "\n";

        }
*/
        return msgError;
    }

    public static boolean mismaSemana(Date d1, Date d2){

        Calendar c = Calendar.getInstance();
        c.setTime(d1);
        c.add(Calendar.DATE,  7 - c.get(Calendar.DAY_OF_WEEK));
        Date domingo1 = c.getTime();

        c.setTime(d2);
        c.add(Calendar.DATE, 7 - c.get(Calendar.DAY_OF_WEEK));
        Date domingo2 = c.getTime();


        if (domingo1.equals(domingo2)){
            return true;
        } else {
            return false;
        }


    }

    public static int numeroReservasAulaSemana(int codAula, String fecha, int codProfesor) {

        int reservasSemana = 0;
        for (Reserva r: MainActivity.getInstanceReservas()){

            if ((r.getCodAula() == codAula) && (r.getCodProfesor() == codProfesor)){
                if (mismaSemana(parseDateEspanol(fecha), parseDateEspanol(r.getFecha()))){
                    reservasSemana++;
                }
            }
        }

        return reservasSemana;

    }

    public static String traducirFechaIngles(String fecha){

        Date d= parseDateEspanol(fecha);
        SimpleDateFormat dmyFormat = new SimpleDateFormat(FORMATO_FECHA_INGLES);
        return dmyFormat.format(d);

    }

    public static String traducirFechaEspañol(String fecha){

        Date d= parseDateIngles(fecha);
        SimpleDateFormat dmyFormat = new SimpleDateFormat(FORMATO_FECHA_ESPANOL);
        return dmyFormat.format(d);

    }


    private void enviarReserva(){

            RequestParams paramsPost = new RequestParams();
            paramsPost.put("cod_profesor", profesor.getText());
            paramsPost.put("cod_aula", aula.getText());
            paramsPost.put("fecha", traducirFechaIngles(fecha.getText() + ""));
            paramsPost.put("inicio", hora.getText());
            paramsPost.put("fin", sumaHora(hora.getText() + ""));
            paramsPost.put("contrasena", contrasena.getText());
            FicherosRed.postAsyncHttp(URL_RESERVAS, paramsPost, this);
            mostrarBarraProgreso();


    }


    @Override
    public void onClick(View v) {

        reserva = new Reserva();
        reserva.setCodProfesor(Integer.parseInt(profesor.getText() + ""));
        reserva.setCodAula(Integer.parseInt(aula.getText() + ""));
        reserva.setFecha(fecha.getText() + "");
        reserva.setHoraInicio(hora.getText() + "");
        reserva.setHoraFinal(sumaHora(hora.getText() + "") + "");

        String restricciones = restriccionesLocales(reserva);
        if (restricciones == "") {

            enviarReserva();


        } else {

            mostrarDialogo("Problemas creando reservas", restricciones);
            Toast.makeText(this,restricciones,Toast.LENGTH_LONG).show();

        }
    }

    public void analizarResultado(Resultado resultado) throws JSONException {


        JSONObject jsonObjeto = new JSONObject(resultado.getContenido());
        String msg = jsonObjeto.getString("message");
        int status = jsonObjeto.getInt("status");
        if (status==MainActivity.OK) {
            reserva.setId(jsonObjeto.getInt("reservas"));
            MainActivity.anadirReserva(reserva, this.getApplicationContext());
            Toast.makeText(this,"Reserva realizada",Toast.LENGTH_SHORT).show();
        } else if (status == MainActivity.NOT_COMPLETED){
            Toast.makeText(this, "Problema desconocido insertando.Inténtalo más tarde",Toast.LENGTH_LONG).show();
        } else if (status == MainActivity.CONFLICT){
            if (msg.contains("Mas de dos reservas en la misma semana")){
                Toast.makeText(this, "No puede reservar más de dos veces el mismo aula durante la misma semana", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Clase ya reservada. La clase ya está reservada a la hora y fecha requerida", Toast.LENGTH_LONG).show();
            }        } else if (status == MainActivity.NOT_PASSWORD){
            Toast.makeText(this, "Contraseña errónea. Introduce de nuevo la contraseña", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onConnectionEnd(Resultado resultado) {

        progreso.dismiss();

            try {

                analizarResultado(resultado);
                //
            } catch (JSONException e) {
                e.printStackTrace();
            }



    }
}
