package com.practicas.miguel.reservasaulas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.practicas.miguel.reservasaulas.libreria.ConnectionListener;
import com.practicas.miguel.reservasaulas.libreria.FicherosRed;
import com.practicas.miguel.reservasaulas.libreria.Resultado;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class BuscarAulaRangoFechasActivity extends Activity implements View.OnClickListener, ConnectionListener{

    public static String  URL_RESERVAS = "http://192.168.1.134/html/slimrest/api/v1/reservas";
    EditText aula, fecha1, fecha2;
    TextView res;
    Button buscar;
    ListView lista;
    ArrayList<Reserva> listaReservas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_aula_rango_fechas);

        aula = (EditText) findViewById(R.id.ARFaula_et);
        fecha1 = (EditText) findViewById(R.id.ARFfecha1_et);
        fecha2 = (EditText) findViewById(R.id.ARFfecha2_et);
        buscar = (Button) findViewById(R.id.ARFbuscar_btn);
        res = (TextView) findViewById(R.id.ARFres_et);
        lista = (ListView) findViewById(R.id.ARFlista_lv);

        buscar.setOnClickListener(this);

        listaReservas = new ArrayList<Reserva>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ver_reservas, menu);
        return true;
    }

    public void obtenerDatos(String url){

        String urlDatos = url + "/" + fecha1.getText() + "/" + fecha2.getText() + "/" +  aula.getText();
        FicherosRed.conectarAsyncHttp(urlDatos, this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private ArrayList<Pair<String, String>> calcularHorasLibres(){
        Date f1 = CrearReservaActivity.parseDateEspanol(fecha1.getText() + "");
        Date f2 = CrearReservaActivity.parseDateEspanol(fecha2.getText() + "");

        ArrayList<Pair<String,String>> h = CrearReservaActivity.generarHorariosEntreDosFechas(f1, f2);

        for (Reserva r : listaReservas){

            h.remove(new Pair(r.getFecha(), r.getHoraInicio()));

        }

        return h;
    }


    private void analizarResultado(Resultado resultado) throws JSONException {


        JSONObject jsonObjeto = new JSONObject(resultado.getContenido());


        JSONArray reservas = jsonObjeto.getJSONArray("reservas");

        for (int i = 0;i<reservas.length();++i){

            Reserva r = new Reserva();
            JSONObject json = reservas.getJSONObject(i);
            r.setId(json.getInt("ID"));
            r.setCodAula(json.getInt("AULA"));
            r.setCodProfesor(json.getInt("PROFESOR"));
            r.setFecha(json.getString("FECHA"));
            r.setHoraInicio(json.getString("HORA_INICIO"));
            r.setHoraFinal(json.getString("HORA_FIN"));
            listaReservas.add(r);

        }


        final ArrayList<Pair<String,String>> h = calcularHorasLibres();

        String [] hString = new String[h.size()];
        int i= 0;
        for (Pair<String, String> p: h){
            hString[i] = "Aula: " + aula.getText() + " " +  p.first +  " " + " " + p.second ;
            ++i;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, hString);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long n) {

                //Abro la reserva a modificar por el usuario.
                Intent intent = new Intent(BuscarAulaRangoFechasActivity.this, CrearReservaActivity.class);
                Reserva res = new Reserva();
                res.setCodAula(Integer.parseInt(aula.getText()+""));
                res.setHoraInicio(h.get(position).second);
                res.setFecha(h.get(position).first);
                intent.putExtra(CrearReservaActivity.RESERVA, res);
                startActivity(intent);


            }
        });




    }


    public void mostrarResultado(){

        String resFinal = "Horas reservadas para el aula " + aula.getText() + ": \n";

        //Basura de método
        for (Reserva r: listaReservas){
            resFinal = resFinal + r.getHoraInicio() + " - " + r.getHoraFinal() + "\n";
        }

        res.setText(resFinal);


    }

    @Override
    public void onConnectionEnd(Resultado resultado) {

        if (resultado.getCodigo()){
            try {
                analizarResultado(resultado);
                mostrarResultado();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

        }


    }

    @Override
    public void onClick(View v) {

        if (v == buscar){
            obtenerDatos(URL_RESERVAS);
        }

    }
}
