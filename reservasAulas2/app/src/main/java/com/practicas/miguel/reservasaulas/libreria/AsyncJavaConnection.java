package com.practicas.miguel.reservasaulas.libreria;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


/**
 * Created by miguel on 12/11/2015.
 */
public class AsyncJavaConnection extends AsyncTask<String, Void, Resultado> {
    public static final String JAVA = "javanet";
    public static final String APACHE = "apache";
    public static final String ASYNCHTTP = "asynchttp";

    private ProgressDialog progreso;
    public ConnectionListener listener;
    public Context context;

    protected void onPreExecute() {
        progreso = new ProgressDialog(context);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(false);
        progreso.show();
    }



    protected Resultado doInBackground(String... cadena) {
        Resultado resultado = null;
        try {


            if (cadena[1]== JAVA)
                   resultado = FicherosRed.conectarJava(cadena[0]);

            if (cadena[1]==APACHE)
                   resultado = FicherosRed.conectarApache(cadena[0]);



        } catch (Exception e) {
            Log.e("HTTP", e.getMessage(), e);
            resultado = null;
            cancel(true);
        }
        return resultado;
    }
    protected void onPostExecute(Resultado result) {

            progreso.dismiss();
            listener.onConnectionEnd(result);


    }
    protected void onCancelled() {
        progreso.dismiss();
    }




}