package com.practicas.miguel.reservasaulas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.practicas.miguel.reservasaulas.libreria.UtilidadesFicheros;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {

    Button bt1, bt2, bt3, bt4, bt5, bt6;

    EditText url_et;
    private static ArrayList<Reserva> misReservas;

    public static final int OK = 200;
    public static final int NOT_COMPLETED = 202;
    public static final int NOT_PASSWORD = 410;
    public static final int CONFLICT = 409;


    public static ArrayList<Reserva> getInstanceReservas(){
        if (misReservas==null){
            misReservas = new ArrayList<Reserva>();
        }
        return misReservas;
    }

    public static final String FICHERO = "reservas.dat";

    public static File getFichero(){
              return  UtilidadesFicheros.abrirFicheroMemoriaExterna(FICHERO);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        bt1 = (Button) findViewById(R.id.main_button1);
        bt2 = (Button) findViewById(R.id.main_button2);
        bt3 = (Button) findViewById(R.id.main_button3);
        bt4 = (Button) findViewById(R.id.main_button4);
        bt5 = (Button) findViewById(R.id.main_button5);
        bt6 = (Button) findViewById(R.id.main_button6);
        url_et = (EditText) findViewById(R.id.main_tv);

        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);
        bt5.setOnClickListener(this);
        bt6.setOnClickListener(this);



        File file = getFichero();

        url_et.setText(CrearReservaActivity.URL_RESERVAS);
        misReservas =  UtilidadesFicheros.cargarObjetoFichero(file,this);
        if (misReservas == null){
            misReservas = new ArrayList<Reserva> ();
        }
    }

    public static void anadirReserva(Reserva reserva, Context context){
        misReservas.add(reserva);
        UtilidadesFicheros.escribirObjetoFichero(getFichero(), misReservas, context);
    }

    public static void eliminarReserva(Reserva reserva, Context context){
        for (Reserva r: misReservas) {
            if (reserva.getId() == r.getId()) {
                misReservas.remove(r);
            }
        }

        UtilidadesFicheros.escribirObjetoFichero(getFichero(), misReservas, context);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onClick(View v) {
        Intent i;
        if (v == bt1){
            i=new Intent(this, VerReservasActivity.class);
            startActivity(i);
        } else if (v == bt2) {
            i = new Intent(this, BuscarReservasAulaFechaActivity.class);
            startActivity(i);
        }else if (v == bt3){

            i=new Intent(this, BuscarAulaRangoFechasActivity.class);
            startActivity(i);

        } else if (v == bt4){
            i=new Intent(this, CrearReservaActivity.class);
            startActivity(i);

        }else if (v == bt5){

            i=new Intent(this, ModificarReservasListaActivity.class);
            startActivity(i);
        } else if ( v==bt6){

            BuscarAulaRangoFechasActivity.URL_RESERVAS = url_et.getText()+"";
            BuscarReservasAulaFechaActivity.URL_RESERVAS = url_et.getText()+"";
            CrearReservaActivity.URL_RESERVAS = url_et.getText()+"";
            ModificarReservaActivity.URL_RESERVAS = url_et.getText()+"";
            ModificarReservasListaActivity.URLRESERVAS = url_et.getText() + "";
            VerReservasActivity.URLRESERVAS = url_et.getText() + "";

        }



    }
}
