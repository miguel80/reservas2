package com.practicas.miguel.reservasaulas;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.practicas.miguel.reservasaulas.libreria.ConnectionListener;
import com.practicas.miguel.reservasaulas.libreria.FicherosRed;
import com.practicas.miguel.reservasaulas.libreria.Resultado;

import org.json.JSONException;
import org.json.JSONObject;

public class ModificarReservaActivity extends Activity implements View.OnClickListener, ConnectionListener {

    public static  String URL_RESERVAS = "http://192.168.1.134/html/slimrest/api/v1/reservas";
    public static final String RESERVA = "RESERVAS";

    private ProgressDialog progreso;
    String password;
    TextView id, profesor;
    EditText contrasena, aula, fecha, hora;
    Button modificar, borrar;
    private Reserva reserva;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_reserva);


        profesor = (TextView) findViewById(R.id.MRprofesor_tv);
        aula  = (EditText) findViewById(R.id.MRcodigoaula_et);
        fecha = (EditText) findViewById(R.id.MRfecha_et);
        hora = (EditText) findViewById(R.id.MRhora_et);
        id = (TextView) findViewById(R.id.MRreserva_tv);
        contrasena = (EditText)findViewById(R.id.MRcontrasena_et);
        modificar = (Button) findViewById(R.id.MRmodificar_btn);
        modificar.setOnClickListener(this);
        borrar = (Button) findViewById(R.id.MRborrar_bt);
        modificar.setOnClickListener(this);
        borrar.setOnClickListener(this);


        Reserva res = (Reserva) getIntent().getSerializableExtra(RESERVA);

        if (res != null) {
            profesor.setText(res.getCodProfesor()+"");
            aula.setText(res.getCodAula()+"");
            fecha.setText(res.getFecha()+"");
            hora.setText(res.getHoraInicio()+"");
            id.setText(res.getId()+"");
        }

    }

    private void mostrarBarraProgreso(){
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(false);
        progreso.show();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crear_reserva, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void mostrarDialogo(String titulo, String mensaje){


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(titulo);
        alert.setMessage(mensaje);

        // Set an EditText view to get user input
        /*final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        alert.setView(input);*/

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //password = input.getText().toString();
                //Log.d( TAG, "Pin Value : " + value);

                return;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                // password = null;
                return;
            }
        });
        alert.show();
    }


    private void enviarReserva(){

        RequestParams paramsPost = new RequestParams();
        paramsPost.put("cod_profesor", profesor.getText());
        paramsPost.put("cod_aula", aula.getText());
        paramsPost.put("fecha", CrearReservaActivity.traducirFechaIngles(fecha.getText() + ""));
        paramsPost.put("inicio", hora.getText());
        paramsPost.put("fin", CrearReservaActivity.sumaHora(hora.getText() + ""));
        paramsPost.put("contrasena", contrasena.getText());
        paramsPost.put("id", id.getText());
        paramsPost.put("delete", 0);
        FicherosRed.putAsyncHttp(URL_RESERVAS, paramsPost, this);
        mostrarBarraProgreso();

    }


    private void borrarReserva(){
        RequestParams paramsPost = new RequestParams();
        paramsPost.put("cod_profesor", profesor.getText());
        paramsPost.put("cod_aula", aula.getText());
        paramsPost.put("fecha", CrearReservaActivity.traducirFechaIngles(fecha.getText() + ""));
        paramsPost.put("inicio", hora.getText());
        paramsPost.put("fin", CrearReservaActivity.sumaHora(hora.getText() + ""));
        paramsPost.put("contrasena", contrasena.getText());
        paramsPost.put("id", id.getText());
        paramsPost.put("delete", 1);
        FicherosRed.putAsyncHttp(URL_RESERVAS, paramsPost, this);;
        mostrarBarraProgreso();
    }

    @Override
    public void onClick(View v) {

        reserva = new Reserva();
        reserva.setCodProfesor(Integer.parseInt(profesor.getText() + ""));
        reserva.setCodAula(Integer.parseInt(aula.getText() + ""));
        reserva.setFecha(fecha.getText() + "");
        reserva.setHoraInicio(hora.getText() + "");
        reserva.setHoraFinal(CrearReservaActivity.sumaHora(hora.getText() + "") + "");


        if (v == modificar) {

            String restricciones = CrearReservaActivity.restriccionesLocales(reserva);
            if (restricciones == "") {

                enviarReserva();


            } else {

                mostrarDialogo("Errores en la creación de la reserva", restricciones);
                Toast.makeText(this, restricciones, Toast.LENGTH_LONG).show();

            }
        }

        if (v == borrar) {

            borrarReserva();
        }
    }


    private void pedirContrasena(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Contraseña Profesor");
        alert.setMessage("Introduce la contraseña para: " + profesor.getText() + ":");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                password = input.getText().toString();
                //Log.d( TAG, "Pin Value : " + value);
                return;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                password = null;
                return;
            }
        });
        alert.show();
    }


    public void analizarResultado(Resultado resultado) throws JSONException {


        JSONObject jsonObjeto = new JSONObject(resultado.getContenido());
        int status = jsonObjeto.getInt("status");
        String msg = jsonObjeto.getString("message");
        if (status==MainActivity.OK) {

            if (msg == "Modificado") {
                MainActivity.eliminarReserva(reserva, this.getApplicationContext());
                MainActivity.anadirReserva(reserva, this.getApplicationContext());
            } else {
                MainActivity.eliminarReserva(reserva, this.getApplicationContext());

            }
                Toast.makeText(this,"Operación realizada con éxito.",Toast.LENGTH_SHORT).show();
        } else if (status == MainActivity.NOT_COMPLETED){
            Toast.makeText(this, "Problema desconocido insertando.Inténtalo más tarde",Toast.LENGTH_LONG).show();
        } else if (status == MainActivity.CONFLICT){
            if (msg.contains("Mas de dos reservas en la misma semana")){
                Toast.makeText(this, "No puede reservar más de dos veces el mismo aula durante la misma semana", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Clase ya reservada. La clase ya está reservada a la hora y fecha requerida", Toast.LENGTH_LONG).show();
            }
        } else if (status == MainActivity.NOT_PASSWORD){
            Toast.makeText(this, "Contraseña errónea. Introduce de nuevo la contraseña", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onConnectionEnd(Resultado resultado) {
        progreso.dismiss();
        try {
            analizarResultado(resultado);
        } catch (Exception e) {
            Toast.makeText(this, "Problema desconocido insertando.Inténtalo más tarde",Toast.LENGTH_LONG).show();

        }
    }
}
